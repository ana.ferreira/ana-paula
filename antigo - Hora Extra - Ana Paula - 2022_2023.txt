Horas Extras  - Ana Paula


15/03/2022 - Total 01:10
17:50 as 19:00 - Analise dos campos de S2306 conforme a nova atualização do layout

16/03/2022 - 00:28 + 00:50 + 02:28 = Total 03:46
07:32 as 08:00 - Analise dos campos do layout S2300
17:50 as 18:40 - Telefone com o Vinicius
21:15 as 23:43 - Analise do Bug que foi encontrado em Ponte Nova passado pelo Serra Negra

17/03/2022 - 00:40 + 01:34 + 01:33 = Total 03:47
07:20 as 08:00 - Subindo correção do Bug que foi encontrado em Ponte Nova pelo Serra Negra em homologacao e demonstração
17:50 as 19:24 - Corrigindo erro da demanda SIP-1386 pre 63739
21:03 as 22:36 - Corrigindo erro da demanda SIP-1386 pre 63739

18/03/2022 -  Total 00:30
07:30 as 08:00 - Liberação da demanda SIP-1386 pre 63739 para testes

Total Geral 
	09:13



Total Geral 
	-00:50

27/06/2022 - Total 01:40
18:00 as 19:40 - Resteste de todo o fluxo do do S2200 e S2300

28/06/2022 - Total 00:40
07:20 as 08:00 - Ajuste da planilha de dados para configuração do SIAP WEB (Esocial)

29/06/2022 - Total 00:42
18:00 as 18:42 - Avaliando de pré carga da contabilidade na folha e tirando duvida do Vinicius e Dani (backend quebrado)


Total Geral 
	02:12

26/07/2022 - 01:21 + 02:28 = Total 03:49
 - 18:00 as 19:21 - Correção do modo debug na VM da folha de pagamento
 - 20:45 as 23:13 - Avaliação da demanda dos evento S2230_AFa e S2230_FER

27/07/2022 - 02:09 + 02:35 = Total 04:44
18:00 as 20:09 - Ajudando a Dani a fazer o restore da base de Tres Marias postgres e configurar o esocial para essa base
21:00 as 23:35 - Correção dos reportes do S2230_AFA

28/07/2022 - 00:32 + 01:11 = Total 01:43
18:00 as 18:32 - Ajustes na tela de classificação de verba pré 65695 
20:33 as 21:44 - Ajustes na tela de classificação de verba pré 65695 

08/08/2022 - 01:00
18:00 as 19:00 - Avaliação dos erros do formaulario de afastamento

10/08/2022 - 01:36
19:44 as 21:20 - Correção do Relatorio de Incidencias S1010

11/08/2022 - 00:44 + 03:38 + 00:53 = Total 05:15 
18:00 as 18:44 - Orientação Rodolfo em Prefeitura de Ipanema
19:12 as 22:50 - Correção e acompanhamento da Prefeitura de Ipanema com Rodolfo e Ana Paula Santos.
23:03 as 23:56 - Sincronização da bases de homologação/demonstração com o hotfix

12/08/2022 - 00:44 
18:00 as 18:44 - Correção do bug de Produção Vinicius/Serra Negra/Pedro

13/08/2022 - 03:27 + 01:45 = Total 05:12
09:03 as 12:30 - Correção do erro da nova versão gerada em produção
14:30 as 16:15 - Testes da correção do Bug Fix.

19/08/2022 - 00:44 + 02:13 = 02:57
18:00 as 18:44 - Acompanhamento de importação de PM de Tres Marias com Rodolfo
19:35 as 21:48 - Geração de Script para PM de Aguas Formosas ESO-1310

20/08/2022 - 00:43
08:50 as 09:33 - Subida de código para produção ESO-1300


Total  27:43

Total Geral 
	29:55
Pagamento das horas extras.

07/09/2022 - 03:24
09:20 as 12:44 - Correção de  demonstração junto com Serra Negra/Vinicius/Pedro

15/09/2022 - 03:15
18:25 as 21:40 - Liberação do S2230_FER e ajustes

22/09/2022 - 01:46
18:00 as 19:46 - Ajuste e dessalocação dos arquivos na VO, carga/formulario/relatorio

Total 08:25


26/10/2022 - 01:30
18:10 as 19:40 - Correção do relatorio de Dependentes apenas Erro

07/11/2022 - 00:35 + 03:25 = Total 04:00
18:10 as 18:45 - Reunião com o Anderson para montar as tabelas de S1210
19:30 as 22:50 - Continuação do desenvolvimento de S1200, e fazer os S1202, S1207

08/11/2022 - 00:35 + 03:20 - Total = 03:55
18:40 as 20:33 - Correção de relatorios S1200
21:15 as 22:30 - Atualização de SQL S1200, para trazer junto as verbas.

09/11/2022 - 01:30 + 01:45 + 00:35 - Total = 03:50
18:10 as 19:40 - Monitoramento dos bancos conectados no servidor 192.168.1.95
20:30 as 22:15 - Ajuste trazendo verbas base conforme solicitação do Serra Negra
22:15 as 22:50 - Atualização de Branch eso-1353 (para mapeamento de lista de lista)

16/11/2022 - Total = 01:34
18:10 as 19:44 - correção de relatório S1202

17/11/2022 - 03:13 + 01:52 - Total = 05:05
18:00 as 21:13 - Tentando corrigir a lista recursiva/Anderson
22:08 as 00:00 - Tentando corrigir a lista recursiva/Anderson

18/11/2022 - 00:44 + 03:33 + 01:35 = Total 05:52
00:00 as 00:44 - Tentando corrigir a lista recursiva/Anderson
18:00 as 21:33 - Tentando corrigir a lista recursiva/Anderson
22:05 as 23:40 - Tentando corrigir a lista recursiva/Anderson

Total Geral - 25:45

08/12/2022 - Total 01:34
18:00 as 19:34 - Correção da consulta de S1202

09/12/2022 - 00:40 + 01:54 = Total 02:34
18:00 as 18:40 - Correção/testes da consulta S1202
21:22 as 23:16 - Correção/testes da consulta S1202

11/12/2022  - 02:40 + 02:15 = Total 04:55
10:20 as 13:00 - Implementação do evento S1207
15:40 as 17:55 - Implementação do evento S1207

12/12/2022 - Total 02:51
21:08 as 23:59 - Correção do S2400 que não esta enviando com sucesso, S1207 formatando xml

13/12/2022  - Total 01:12
00:01  as 01:13 - S1207 formatando xml, teste de envio S1207 (Envio com sucesso \o/)

13/12/2022 - 01:29 + 01:56 = Total  03:25
19:40 as 21:09 - Melhoria dos DmDev´s conforme alinhamento do Serra Negra e Pedro, para mais de uma matricula no mesmo CUC
21:48 as 23:44 - Melhoria dos DmDev´s conforme alinhamento do Serra Negra e Pedro, para mais de uma matricula no mesmo CUC/Testes

14/12/2022 - 00:43 + 01:57 = Total 02:40
18:00 as 18:43 - Reunião com Serra Negra eventos S1200 dmdev
21:12 as 23:09 - Correção SQL S1200, SQL de FichaFinanceira, Correção tpPgto e retificação

16/12/2022 - 01:25 + 01:31 = Total 02:56
18:00 as 19:25 - feat(eso-1353): Correção chaves S1210
19:44 as 21:15 - feat(eso-1353): Correção chaves S1210/testes/liberacao


Total Geral 22:07


++++++ 25:45


Total Geral 47:52
Recebido

Total Geral -08:48

01/03/2023 - 02:11 + 00:48 = Total 02:59
18:33 as 20:44 - Correção de redis para layout S2210
21:02 as 21:50 - Correção de redis para layout S2210

02/03/2023 - 01:53 + 02:29 = Total 04:22
18:00 as 19:53 - Testes S2210 para liberação do evento
20:15 as 22:44 - Finalização dos teste para liberação do S2210

14/03/2023 - 01:14 + 01:36 = Total 02:50
18:42 as 19:56 - Debug S1200 informações complementares
20:24 as 22:00 - Debug S1200 informações complementares

16/03/2023 - Total 01:12
18:00 as 19:12 - Chamado PM de Pains

Total 11:23 - 08:48 = 02:35

Total Geral = 02:35



20/03/2023 - 01:40 + 02:11 = Total 03:51
18:00 as 19:40 - Report S2210
20:44 as 22:55 - Reporte S2210/ Liberação para testes

29/03/2023 - 01:21 + 02:41 = Total 03:51
18:00 as 19:21 - Correção de percorrer o mapa nova implantação dos evento recursivos
20:07 as 22:48 - Correção de percorrer o mapa nova implantação dos evento recursivos

31/03/2023 - 02:25 + 01:36 = Total 04:10
18:00 as 20:25 - feat(eso-1853): Correção consulta S1200 Autonomos
20:54 as 22:30 - feat(eso-1853): Correção consulta S1200 Autonomos

03/04/2023 - 01:36 + 01:36 + 00:24 = Total 03:36
18:00 as 19:26 - !1802 feat(1853): Criação de novas cargas para S1200 autonomos	
19:26 as 21:02 - !1803 feat(eso-1587): Correçaõ consulta S2210
21:20 as 21:44 - Subida de Hotfix FRONT, Reiniciando Redis produção, Reninicando Aplicação esocial produção

04/04/2023 - 01:25 + 00:23 = Total 01:48
18:00 as 19:25 - Teste da correção do relatorio de S1200 em produção e criação do merge (feat: (eso-2010): Correção relatórios S5001, S5002 e S5003)
20:52 as 21:15 - Disparo do papeline para pegar merge de correção relatorio

10/04/2023 - Total 01:59
18:00 as 19:59 - Implementação de Carga Nova de S1010

13/04/2023 - Total 01:34
18:00 as 19:34 - (eso-1833) - Retirada de InfoComplem para categoria 701, S1200 autonomos

Total Geral = 20:49


Total Geral = 02:35 + 20:49 = 23:24


28/04/2023 - 01:33 + 00:12 = Total 01:45
19:07 as 20:40 - Liberação de Hotfix - fix(eso-2085): Alteração na mudança de vigência do S1010 (merge)
21:33 as 21:45 - Reiniciação do redis, mais acompanhamento do merge em produção

01/05/2023 - 04:42 + 00:40 = Total 05:22
10:07 as 14:49 - feat(eso-1912): construção do evento S1210 Autônomos
15:55 as 16:35 - feat(eso-1912): Teste de importação

19/06/2023 - 01:36 + 00:56 = Total 02:32
20:43 as 22:19 - Correção de consulta, nome responsavel eso-1584
22:19 as 23:15 - Avaliação do hotfix eso-2234, campo nulo no S2299

Total Geral = 09:39

Total Geral = 23:24 + 09:39 = 33:30

Desconto emenda feriado 09/06/2023 - 08:48

Total Geral = 33:30 - 08:48 = 24:42


11/09/2023 - Total 00:33
20:00 as 20:33 - Correção dos travamentos de importação esocial

12/09/2023 - 01:18 + 00:54 = Total 02:12
18:00 as 19:18 - Correção e conclusão de bugs CARDs IESO-220, IESO-371, IESO-395
22:54 as 23:48 - Correção e conclusão dos bugs CARDs IESO-378, IESO-381 e IESO-390


13/09/2023 - 01:56 + 01:11 = Total 03:07
18:00 as 19:56 - Correção e conclusão de bugs CARDs IESO-398, IESO-399, IESO-400 e IESO-394
20:44 as 21:55 - Correção e conclusão de bugs CARDs IESO-382, IESO-391 e IESO-402

14/09/2023 - 00:52 + 00:34 = Total 01:26
20:40 as 21:32 - Analise da fila de conexão do esocial WEB, após ajuste da planilha
23:12 as 23:46 - Subido do hotFix feat(ieso-368): Correção lentidão S1210 Autonomos e Ajustes solicitados pela contabilidade, aguardando clientes importando.

Total = 07:18


Total Geral = 24:42 + 07:18 = Total 32:00


20/10/2023 - Total 01:03
18:00 as 19:03 - Ligação com Anderson, repasse de envio A3 e ajuda com a construção do evento S2500.

24/10/2023 - 00:53 + 00:22 = Total 01:15
18:00 as 18:53 - Ligação com Anderson, repasse dos relatorio de inconsistencias de INSS.
19:34 as 19:56 - Ajuste relatorio de INSS ieso-451

26/10/2023 - 00:42 + 00:47 = Total 01:29 
18:00 as 18:42 - Correção e teste do hotfix eso-529 junto com Serra Negra
22:48 as 23:35 - Geração de versão para hotfix contabilidade ieso-529, erro na geração

27/10/2023 - Total 00:22
06:15 as 06:37 - Build novamente para gerar versao contabilidade ieso-529

31/10/2023 - 01:26 + 01:36 = Total 03:02
18:00 as 19:26 - ieso-511 e ieso-530 envio para teste ajustes contabilidade
21:36 as 23:12 - ieso-511 e ieso-530 envio para teste ajustes contabilidade e verificação relatorio ieso-451


Total Geral: 07:11


04/12/2023 - 00:22 + 01:45 = Total 02:07
17:00 as 17:22 - Teste da feat(ieso-617): Tratamento de processamento nulo - IESO-617
22:13 as 22:41 - Reiniciação e acompanhamento da subida do hotfix ieso-617
22:41 as 23:06 - Reinicialização do Login para conexão de Instituto Previdencia Nova Mamore IESO-579
23:06 as 23:58 - Correção de novos campos S2200, S2205, S2206 e S2400

07/12/2023 - 01:33 + 00:35 = Total 02:08
17:00 as 18:33 - Correção da Pensao alimenticia S1210 Autonomos - IESO-356 e IESO-351
23:10 as 23:45 - Liberação de hotFix PM Carmesia - IESO-636

08/12/2023 - 00:34 + 00:43 = Total 01:17
17:00 as 17:34 - Em ligação com Ana Paula Santos conectada no cliente - IESO-616
22:32 as 23:15 - Rebuild do esocial para ver se pega o hotfix - IESO-636

09/12/2023 - Total 00:35
07:24 as 07:59 - Reinicialização do esocial  e teste de PM Carmesia, informações passadas a Thandessa.

13/12/2023 - 01:06 + 00:31 = Total 01:37
17:00 as 18:06 - Criação do script para correção das verbas que estão com a carga da Folha de Pagamento sem preencher a tabela FOL_RUBLICAESOCIAL - ieso-666.
22:17 as 22:48 - Build do schema.yml e acompanhamento da subida do cliente para configuração de PM Perdoes.

Total: 07:44


Total Geral: 14:55




02/01/2024 - Total 00:26
22:52 as 23:18 - Subida de versão produção.


